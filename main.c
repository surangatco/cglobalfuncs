#include<stdio.h>

#include"class1.h"

int main()
{
	printName();
	//printNameStatic();//we can not call this as this is a static function decalared in class1.h. it can only be used in the class1.c/class1.h 
	callprintNameStatic();//but we can get the results of above static function by calling this function.
	return 0;
}
